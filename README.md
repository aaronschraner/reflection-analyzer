# reflection-analyzer
Hardware for troubleshooting tracking issues caused by reflections on Valve Index and HTC Vive lighthouse-based tracking setups

RJ45-quad contains KiCAD design files for a circuit board that implements a high speed differential transimpedance amplifier, designed to be used with a BPW34 photodiode on the end of a 120 ohm twisted pair cable. 

The board is designed to operate off a 5V power supply, and outputs a single-ended voltage proportional to the current through the photodiode.
Gain of the amplifier is configurable via a gain resistor header, and is set to 2V/uA by default. 

This project is ongoing, but the ultimate goal is to build something that can identify reflection pulses automatically (without the use of an oscilloscope) to aid with troubleshooting reflection-induced tracking problems. The RJ45-quad board is used as a building block in this system.
